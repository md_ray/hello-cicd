package main

import (
	"log"
	"net/http"
)

func HandleResponse(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	w.Write([]byte("{status: 'OK'}"))
}

func main() {
	http.HandleFunc("/", HandleResponse)

	log.Println("http server running at localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
